module com.gitlab.openpresenter {

	requires transitive javafx.graphics;

	requires transitive javafx.media;

	requires javafx.controls;

	requires javafx.fxml;

	requires javafx.base;

	requires atlantafx.base;

	requires org.kordamp.ikonli.javafx;

	requires org.kordamp.ikonli.bootstrapicons;

	requires spring.boot.autoconfigure;

	requires spring.context;

	requires spring.beans;

	requires spring.boot;

	requires jakarta.annotation;

	requires org.slf4j;

	requires java.prefs;

	requires static lombok;

	opens com.gitlab.openpresenter.controls.media to spring.core;

	opens com.gitlab.openpresenter.controls.presentation to spring.core;

	opens com.gitlab.openpresenter.views.main to spring.core;

	opens com.gitlab.openpresenter.views.presentation to spring.core;

	opens com.gitlab.openpresenter.model.screen to spring.core;

	opens com.gitlab.openpresenter to spring.core;

	exports com.gitlab.openpresenter.controls.media to spring.beans;

	exports com.gitlab.openpresenter.controls.presentation to spring.beans;

	exports com.gitlab.openpresenter.views.main to spring.beans;

	exports com.gitlab.openpresenter.views.presentation to spring.beans;

	exports com.gitlab.openpresenter.model.presentation to spring.beans;

	exports com.gitlab.openpresenter.model.screen to spring.beans;

	exports com.gitlab.openpresenter;

}
