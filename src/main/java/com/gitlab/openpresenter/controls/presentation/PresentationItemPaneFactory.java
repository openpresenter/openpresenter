/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.controls.presentation;

import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.model.presentation.LyricsPresentationItem;
import com.gitlab.openpresenter.model.presentation.MediaPresentationItem;
import com.gitlab.openpresenter.model.presentation.PresentationItem;
import com.gitlab.openpresenter.model.presentation.SlidesPresentationItem;

import javafx.scene.Node;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class PresentationItemPaneFactory {

	@NonNull
	private final PresentationControlFactory controlFactory;

	public Node createPresentationItemPane(@NonNull final PresentationItem item) {
		return switch (item.getType()) {
		case LYRICS -> new LyricsPresentationItemPane(controlFactory, (LyricsPresentationItem) item);
		case MEDIA -> new MediaPresentationItemPane(controlFactory, (MediaPresentationItem) item);
		case SLIDES -> new SlidesPresentationItemPane(controlFactory, (SlidesPresentationItem) item);
		};
	}

}
