package com.gitlab.openpresenter.controls.presentation;

import org.kordamp.ikonli.bootstrapicons.BootstrapIcons;
import org.kordamp.ikonli.javafx.FontIcon;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.model.screen.ScreenItem;
import com.gitlab.openpresenter.model.screen.ScreenModel;

import atlantafx.base.theme.Tweaks;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import lombok.Getter;
import lombok.NonNull;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ScreenSelector extends HBox {

	private final FontIcon icon;

	@Getter
	private final ComboBox<ScreenItem> comboBox;

	public ScreenSelector(@NonNull final ScreenModel screenModel) {
		setAlignment(Pos.CENTER_LEFT);
		setPadding(new Insets(0, 4, 0, 4));
		setSpacing(12);

		icon = new FontIcon(BootstrapIcons.DISPLAY);

		comboBox = new ComboBox<>();
		comboBox.setItems(screenModel.getScreens());
		comboBox.getStyleClass().add(Tweaks.ALT_ICON);

		getChildren().addAll(icon, comboBox);

		widthProperty().subscribe(() -> comboBox.setPrefWidth(getWidth()
				- icon.getIconSize()
				- getSpacing()
				- getPadding().getLeft()
				- getPadding().getRight()));
	}

	public void fillWidth(final double width) {
		comboBox.setPrefWidth(width
				- icon.getIconSize()
				- getSpacing()
				- getPadding().getLeft()
				- getPadding().getRight());
	}

}
