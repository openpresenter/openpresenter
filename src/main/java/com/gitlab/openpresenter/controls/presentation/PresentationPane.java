/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.controls.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.model.presentation.PresentationItem;
import com.gitlab.openpresenter.views.presentation.PresentationViewModel;

import jakarta.annotation.PostConstruct;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.layout.StackPane;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PresentationPane extends StackPane {

	@Autowired
	private PresentationItemPaneFactory presentationItemPaneFactory;

	private final ObjectProperty<PresentationItem> currentItem = new SimpleObjectProperty<>();

	public ObjectProperty<PresentationItem> currentItemPropertiy() {
		return currentItem;
	}

	public PresentationItem getCurrentItem() {
		return currentItem.get();
	}

	public void setCurrentItem(final PresentationItem currentItem) {
		this.currentItem.set(currentItem);
	}

	@Autowired
	public void setPresentationViewModel(final PresentationViewModel model) {
		model.presentationWidthPropertiy().subscribe(w -> setPrefWidth(w.doubleValue()));
		model.presentationHeightPropertiy().subscribe(h -> setPrefHeight(h.doubleValue()));
	}

	@PostConstruct
	void init() {
		currentItem.subscribe(this::showPresentationItem);
	}

	private void showPresentationItem(final PresentationItem item) {
		if (item != null) {
			final var pane = presentationItemPaneFactory.createPresentationItemPane(item);

			pane.prefWidth(getPrefWidth());
			pane.prefHeight(getPrefHeight());

			if (getChildren().size() > 0)
				getChildren().set(0, pane);
			else getChildren().add(0, pane);
		} else getChildren().clear();
	}

}
