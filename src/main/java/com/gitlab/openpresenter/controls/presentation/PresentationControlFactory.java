package com.gitlab.openpresenter.controls.presentation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.controls.media.MediaPlayerPane;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import lombok.NonNull;

@Component
public class PresentationControlFactory {

	private final Font fontHeader = Font.font("Verdana", FontWeight.BOLD, 72);

	private final Font fontLyrics = Font.font("Verdana", FontWeight.BOLD, 54);

	private final Font fontSlides = Font.font("Verdana", FontWeight.BOLD, 54);

	@Autowired
	private ObjectProvider<MediaPlayerPane> mediaPlayerPaneFactory;

	public Node createHeader(@NonNull final String title) {
		final var label = new Label(title);
		label.setFont(fontHeader);
		label.setTextFill(Color.YELLOW);
		return label;
	}

	public List<Node> createLyrics(@NonNull final String[] lines) {
		return createLyrics(0, lines);
	}

	public List<Node> createLyrics(final int number, @NonNull final String[] lines) {
		final var length = lines.length;
		final var labels = new ArrayList<Node>(length);

		for (var i = 0; i < length; ++i) {
			final var text = number > 0 && i == 0
					? number + ". " + lines[i]
					: lines[i];

			final var label = new Label(text);
			label.setFont(fontLyrics);
			label.setTextFill(Color.WHITESMOKE);
			labels.add(label);
		}

		return labels;
	}

	public List<Node> createSlides(@NonNull final String[] lines) {
		return createSlides(0, lines);
	}

	public List<Node> createSlides(final int number, @NonNull final String[] lines) {
		final var length = lines.length;
		final var labels = new ArrayList<Node>(length);

		for (var i = 0; i < length; ++i) {
			final var text = number > 0 && i == 0
					? number + ". " + lines[i]
					: lines[i];

			final var label = new Label(text);
			label.setFont(fontSlides);
			label.setTextFill(Color.WHITESMOKE);
			labels.add(label);
		}

		return labels;
	}

	public Node createMedia(@NonNull final MediaPlayer mediaPlayer) {
		return mediaPlayerPaneFactory.getObject(mediaPlayer);
	}

}
