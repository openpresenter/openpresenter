/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.controls.media;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.views.presentation.PresentationViewModel;

import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MediaPlayerPane extends BorderPane {

	private final MediaView mediaView;

	public MediaPlayerPane(final MediaPlayer mediaPlayer) {
		setBackground(Background.fill(Color.BLACK));

		mediaView = new MediaView();
		mediaView.setPreserveRatio(true);
		mediaView.setSmooth(true);
		mediaView.setMediaPlayer(mediaPlayer);
		setCenter(mediaView);
	}

	@Autowired
	public void setPresentationViewModel(final PresentationViewModel model) {
		model.presentationWidthPropertiy().subscribe(w -> {
			mediaView.setFitWidth(w.doubleValue());
			log.info("update media view size: {}x{}", mediaView.getFitWidth(), mediaView.getFitHeight());
		});
		model.presentationHeightPropertiy().subscribe(h -> {
			mediaView.setFitHeight(h.doubleValue());
			log.info("update media view size: {}x{}", mediaView.getFitWidth(), mediaView.getFitHeight());
		});
	}

}
