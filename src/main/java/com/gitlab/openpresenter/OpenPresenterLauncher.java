/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OpenPresenterLauncher extends Application {

	private ConfigurableApplicationContext appContext;

	@Override
	@SneakyThrows
	public void start(final Stage stage) {
		showSplashScreen(stage);

		appContext = new SpringApplicationBuilder(OpenPresenterApp.class).run();
		log.info("start spring context");

		loadIconsForStage(stage);
	}

	@Override
	@SneakyThrows
	public void stop() {
		log.info("shutdown spring context");
		appContext.close();
		System.exit(0);
	}

	private void showSplashScreen(final Stage stage) {
		// TODO implement splash screen
		stage.close();
	}

	private static void loadIconsForStage(final Stage stage) throws IOException {
		// This is required for Windows and Linux. On macOS there's no distinction
		// between window
		// icons and app icons, so we don't bundle the icon PNGs separately and thus the
		// loop here
		// doesn't do anything.
		final var appDir = System.getProperty("app.dir");
		if (appDir == null)
			return;
		final var iconsDir = Paths.get(appDir);
		try (var dirEntries = Files.newDirectoryStream(iconsDir, "icon-*.png")) {
			for (final Path iconFile : dirEntries) try (var icon = Files.newInputStream(iconFile)) {
				stage.getIcons().add(new Image(icon));
			}
		}
	}

}
