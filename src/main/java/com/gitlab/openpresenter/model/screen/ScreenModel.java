/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.model.screen;

import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.stage.Screen;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ScreenModel {

	@Getter
	private final ObservableList<ScreenItem> screens;

	private final ObjectProperty<ScreenRatio> screenRatio = new SimpleObjectProperty<>(ScreenRatio._16x9);

	private final ObjectProperty<ScreenItem> audienceScreen = new SimpleObjectProperty<>();

	private final ObjectProperty<ScreenItem> stageScreen = new SimpleObjectProperty<>();

	public ScreenModel() {
		screens = new ScreenItemList(Screen.getScreens());
	}

	@PostConstruct
	public void scanScreens() {
		final var primaryScreen = Screen.getPrimary();

		final var screens = Screen.getScreens();
		ScreenItem audienceScreen = null;
		for (var i = 0; i < screens.size() && audienceScreen == null; ++i) {
			final var screen = screens.get(i);
			if (!screen.equals(primaryScreen)) audienceScreen = new ScreenItem(i, screen);
		}

		ScreenItem stageScreen = null;
		for (var i = 0; i < screens.size() && stageScreen == null; ++i) {
			final var screen = screens.get(i);
			if (!screen.equals(primaryScreen) && !screen.equals(audienceScreen.getScreen())) stageScreen = new ScreenItem(i, screen);
		}

		log.info("primary={} audience={} stage={}", screenInfo(primaryScreen), audienceScreen, stageScreen);

		setAudienceScreen(audienceScreen);
		setStageScreen(stageScreen);

		avoidDuplicateAssignment(this.audienceScreen, this.stageScreen);
		avoidDuplicateAssignment(this.stageScreen, this.audienceScreen);
	}

	public ObjectProperty<ScreenRatio> screenRatioProperty() {
		return screenRatio;
	}

	public ScreenRatio getScreenRatio() {
		return screenRatio.get();
	}

	public void setScreenRatio(@NonNull final ScreenRatio screenRatio) {
		this.screenRatio.set(screenRatio);
	}

	public ObjectProperty<ScreenItem> audienceScreenProperty() {
		return audienceScreen;
	}

	public ScreenItem getAudienceScreen() {
		return audienceScreen.get();
	}

	public void setAudienceScreen(final ScreenItem audienceScreen) {
		this.audienceScreen.set(audienceScreen);
	}

	public ObjectProperty<ScreenItem> stageScreenProperty() {
		return stageScreen;
	}

	public ScreenItem getStageScreen() {
		return stageScreen.get();
	}

	public void setStageScreen(final ScreenItem stageScreen) {
		this.stageScreen.set(stageScreen);
	}

	private void avoidDuplicateAssignment(@NonNull final ObjectProperty<ScreenItem> screen1, @NonNull final ObjectProperty<ScreenItem> screen2) {
		screen1.subscribe((previous, current) -> {
			if (previous != current && current != null && current.equals(screen2.get())) {
				screen2.set(null);
			}
		});
	}

	private static String screenInfo(final Screen screen) {
		return screen != null
				? "[" + screen.getBounds().getMinX()
						+ "," + screen.getBounds().getMinY()
						+ "," + screen.getBounds().getWidth()
						+ "," + screen.getBounds().getHeight()
						+ "@" + screen.getDpi() + "]"
				: null;
	}

}
