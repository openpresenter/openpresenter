/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.model.screen;

import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Accessors(fluent = true)
public enum ScreenRatio {

	_16x9(16, 9),
	_16x10(16, 10),
	_4x3(4, 3),
	_3x2(3, 2);

	private final String text;

	private final int width;

	private final int height;

	ScreenRatio(final int width, final int height) {
		if (width <= 0) throw new IllegalArgumentException("invalid width: " + width);
		if (height <= 0) throw new IllegalArgumentException("invalid height: " + height);

		this.text = width + ":" + height;
		this.width = width;
		this.height = height;
	}

}
