package com.gitlab.openpresenter.model.screen;

import javafx.stage.Screen;
import lombok.Getter;
import lombok.NonNull;

@Getter
public class ScreenItem {

	private final int number;

	private final Screen screen;

	private final String name;

	public ScreenItem(final int number, @NonNull final Screen screen) {
		this.number = number;
		this.screen = screen;
		this.name = String.format("Display #%d - %.0f x %.0f px @ %.0f dpi",
				number + 1,
				screen.getBounds().getWidth(),
				screen.getBounds().getHeight(),
				screen.getDpi());
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return screen.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof final ScreenItem other
				? screen.getBounds().equals(other.screen.getBounds())
				: false;
	}

}
