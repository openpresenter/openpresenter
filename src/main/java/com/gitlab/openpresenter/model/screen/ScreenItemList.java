package com.gitlab.openpresenter.model.screen;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.collections.transformation.TransformationList;
import javafx.stage.Screen;

public class ScreenItemList extends TransformationList<ScreenItem, Screen> {

	private final ObservableList<? extends Screen> screens;

	private final List<ScreenItem> items;

	protected ScreenItemList(ObservableList<? extends Screen> source) {
		super(source);
		screens = source;
		items = new ArrayList<>();
	}

	@Override
	public int size() {
		return screens.size();
	}

	@Override
	protected void sourceChanged(Change<? extends Screen> c) {
		while (items.size() > screens.size()) {
			items.removeLast();
		}
	}

	@Override
	public int getSourceIndex(int index) {
		return index;
	}

	@Override
	public int getViewIndex(int index) {
		return index;
	}

	@Override
	public ScreenItem get(int index) {
		final var screen = screens.get(index);
		if (index >= items.size()) {
			final var item = new ScreenItem(index, screen);
			items.add(index, item);
			return item;
		} else {
			var item = items.get(index);
			if (item == null || item.getScreen() != screen || item.getNumber() != index) {
				item = new ScreenItem(index, screen);
				if (index < items.size())
					items.set(index, item);
				else items.add(index, item);
			}
			return item;
		}
	}

}
