/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.model.screen.ScreenModel;
import com.gitlab.openpresenter.views.main.MainViewModel;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class OpenPresenterSettingsService {

	@NonNull
	private final OpenPresenterSettings settings;

	@Autowired
	public void bindScreenModel(@NonNull final ScreenModel model) {
		model.screenRatioProperty().set(settings.getScreenRatio());
		model.screenRatioProperty().subscribe(settings::setScreenRatio);
	}

	@Autowired
	public void bindMainViewModel(@NonNull final MainViewModel model) {
		model.darkModeProperty().set(settings.isDarkMode());
		model.darkModeProperty().subscribe(settings::setDarkMode);

		model.projectDividerPosProperty().set(settings.getScheduleDividerPos());
		model.projectDividerPosProperty().subscribe(pos -> settings.setScheduleDividerPos(pos.doubleValue()));
		model.previewDividerPosProperty().set(settings.getPreviewDividerPos());
		model.previewDividerPosProperty().subscribe(pos -> settings.setPreviewDividerPos(pos.doubleValue()));
		model.libraryDividerPosProperty().set(settings.getLibraryDividerPos());
		model.libraryDividerPosProperty().subscribe(pos -> settings.setLibraryDividerPos(pos.doubleValue()));
	}

}
