/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.model.screen.ScreenItem;
import com.gitlab.openpresenter.model.screen.ScreenModel;

import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;

@Component
public class CalibrationPane extends Pane {

	private final Canvas canvas = new Canvas();

	private String screenInfo;

	public CalibrationPane() {
		getChildren().add(canvas);
	}

	@Autowired
	void setScreenModel(final ScreenModel model) {
		model.audienceScreenProperty().subscribe(this::updateScreen);
	}

	@Override
	protected void updateBounds() {
		super.updateBounds();

		final var w = getWidth();
		final var h = getHeight();
		canvas.setWidth(w);
		canvas.setHeight(h);

		final var g = canvas.getGraphicsContext2D();
		g.clearRect(0, 0, w, h);
		g.setLineWidth(2);

		g.setStroke(Color.DIMGREY);
		g.strokeLine(0, 0, w, h);
		g.strokeLine(0, h, w, 0);

		g.setStroke(Color.ORANGERED);
		g.strokeRect(1, 1, w - 2, h - 2);

		final var d = Math.min(w, h) / 3;
		final var centerX = (w - d) / 2;
		final var centerY = (h - d) / 2;
		g.strokeArc(centerX, centerY, d, d, 0, 360, ArcType.OPEN);

		g.setFill(Color.DEEPSKYBLUE);
		g.setFont(new Font("Arial", 24));
		g.fillText(screenInfo, 10, 34);
	}

	private void updateScreen(final ScreenItem screen) {
		screenInfo = screen != null ? screen.toString() : "";
	}

}
