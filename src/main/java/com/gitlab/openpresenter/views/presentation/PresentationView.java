/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.controls.presentation.PresentationPane;
import com.gitlab.openpresenter.model.screen.ScreenItem;
import com.gitlab.openpresenter.model.screen.ScreenModel;

import jakarta.annotation.PostConstruct;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Background;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

@Component
public class PresentationView extends Stage {

	@Autowired
	private PresentationViewModel presentationViewModel;

	@Autowired
	private ScreenModel screenModel;

	@Autowired
	private PresentationPane presentationPane;

	@Autowired
	private CalibrationPane calibrationPane;

	private StackPane stackPane;

	PresentationView() {
		super(StageStyle.UNDECORATED);

		initModality(Modality.WINDOW_MODAL);
		setAlwaysOnTop(true);
		setResizable(false);
		setFullScreenExitHint("");
		setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
		hide();
	}

	@PostConstruct
	void init() {
		stackPane = new StackPane(presentationPane);
		stackPane.setBackground(Background.EMPTY);
		stackPane.prefWidthProperty().bind(presentationPane.prefWidthProperty());
		stackPane.prefHeightProperty().bind(presentationPane.prefHeightProperty());

		final var topCenterPane = new VBox(stackPane);
		topCenterPane.setAlignment(Pos.TOP_CENTER);
		topCenterPane.setFillWidth(false);
		topCenterPane.setBackground(Background.EMPTY);

		final var scene = new Scene(topCenterPane);
		scene.setFill(Color.BLACK);
		scene.setOnKeyPressed(event -> {
			// Hint: On MacOS it seems to be not possible to disable ESC to exit fullscreen mode, so we will leave the preview better cleanly.
			if (event.getCode() == KeyCode.ESCAPE) screenModel.setAudienceScreen(null);
		});
		setScene(scene);

		screenModel.audienceScreenProperty().subscribe(this::showOnScreen);
		presentationViewModel.calibrationVisiblePropertiy().subscribe(this::showCalibrationPane);
		presentationPane.currentItemPropertiy().bind(presentationViewModel.currentItemPropertiy());
	}

	private void showOnScreen(final ScreenItem screen) {
		if (screen != null) {
			final var bounds = screen.getScreen().getBounds();
			if (getX() != bounds.getMinX() || getY() != bounds.getMinY()) hide();

			Platform.runLater(() -> {
				setX(bounds.getMinX());
				setY(bounds.getMinY());
				setWidth(bounds.getWidth());
				setHeight(bounds.getHeight());
				setFullScreen(true);
				show();
			});
		} else {
			hide();
		}
	}

	private void showCalibrationPane(final boolean calibrationVisible) {
		final var children = stackPane.getChildren();
		if (calibrationVisible) {
			if (!children.contains(calibrationPane)) children.add(calibrationPane);
		} else {
			children.remove(calibrationPane);
		}
	}

}
