/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.model.presentation.PresentationItem;
import com.gitlab.openpresenter.model.screen.ScreenItem;
import com.gitlab.openpresenter.model.screen.ScreenModel;
import com.gitlab.openpresenter.model.screen.ScreenRatio;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PresentationViewModel {

	private final BooleanProperty calibrationVisible = new SimpleBooleanProperty(true);

	private final ObjectProperty<PresentationItem> currentItem = new SimpleObjectProperty<>();

	private final DoubleProperty presentationWidth = new SimpleDoubleProperty();

	private final DoubleProperty presentationHeight = new SimpleDoubleProperty();

	@Autowired
	public void setScreenModel(final ScreenModel screenModel) {
		// note: consumer with single parameter will be called initially
		screenModel.screenRatioProperty().subscribe(ratio -> updateScreen(screenModel.getAudienceScreen(), ratio));

		// take care: bi-consumer with two parameters will be called only when updated
		screenModel.audienceScreenProperty().subscribe((previous, screen) -> {
			if (previous != screen) updateScreen(screen, screenModel.getScreenRatio());
		});
	}

	public BooleanProperty calibrationVisiblePropertiy() {
		return calibrationVisible;
	}

	public boolean getCalibrationVisible() {
		return calibrationVisible.get();
	}

	public void setCalibrationVisible(final boolean calibrationVisible) {
		this.calibrationVisible.set(calibrationVisible);
	}

	public ObjectProperty<PresentationItem> currentItemPropertiy() {
		return currentItem;
	}

	public PresentationItem getCurrentItem() {
		return currentItem.get();
	}

	public void setCurrentItem(final PresentationItem currentItem) {
		this.currentItem.set(currentItem);
	}

	public DoubleProperty presentationWidthPropertiy() {
		return presentationWidth;
	}

	public double getPresentationWidth() {
		return presentationWidth.get();
	}

	public void setPresentationWidth(final double presentationWidth) {
		this.presentationWidth.set(presentationWidth);
	}

	public DoubleProperty presentationHeightPropertiy() {
		return presentationHeight;
	}

	public double getPresentationHeight() {
		return presentationHeight.get();
	}

	public void setPresentationHeight(final double presentationHeight) {
		this.presentationHeight.set(presentationHeight);
	}

	private void updateScreen(final ScreenItem screen, final ScreenRatio ratio) {
		if (screen != null) {
			final var screenWidth = screen.getScreen().getBounds().getWidth();
			final var screenHeight = screen.getScreen().getBounds().getHeight();

			final var prefWidth = Math.round(screenHeight * ratio.width() / ratio.height());
			final var prefHeight = Math.round(screenWidth * ratio.height() / ratio.width());

			log.debug("ratio={} screen={}x{} preferredSize={}x{}", ratio.text(), Math.round(screenWidth), Math.round(screenHeight), prefWidth, prefHeight);

			presentationWidth.set(Math.min(screenWidth, prefWidth));
			presentationHeight.set(Math.min(screenHeight, prefHeight));
		} else {
			presentationWidth.set(0);
			presentationHeight.set(0);
		}
	}

}
