/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.main;

import org.springframework.stereotype.Component;

import atlantafx.base.theme.Styles;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;

@Component
public class LibraryPane extends BorderPane {

	public LibraryPane() {
		getStyleClass().add(Styles.BG_SUBTLE);

		final var lyricsTab = new Tab("Lyrics", new Label(getClass().getSimpleName()));
		lyricsTab.setClosable(false);

		final var bibleTab = new Tab("Bible", new Label(getClass().getSimpleName()));
		bibleTab.setClosable(false);

		final var mediaTab = new Tab("Media", new Label(getClass().getSimpleName()));
		bibleTab.setClosable(false);

		final var tabPane = new TabPane(lyricsTab, bibleTab, mediaTab);
		tabPane.getStyleClass().add(Styles.DENSE);
		setCenter(tabPane);
	}

}
