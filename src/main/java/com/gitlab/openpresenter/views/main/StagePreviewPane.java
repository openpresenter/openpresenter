/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.controls.presentation.ScreenSelector;
import com.gitlab.openpresenter.model.screen.ScreenModel;

import jakarta.annotation.PostConstruct;
import javafx.scene.Node;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.VBox;

@Component
public class StagePreviewPane extends VBox {

	@Autowired
	private ScreenModel screenModel;

	@Autowired
	private ScreenSelector screenSelector;

//	@Autowired
//	private MediaPlayerPane mediaPlayerPane;

	@PostConstruct
	void init() {
		setFillWidth(true);
		getChildren().addAll(createScreenSelector());
//		getChildren().addAll(createScreenSelector(), mediaPlayerPane);
//		widthProperty().subscribe(width -> mediaPlayerPane.setPrefHeight(width.doubleValue() * 3d / 4d));

		screenModel.stageScreenProperty().subscribe(screenSelector.getComboBox().getSelectionModel()::select);
		screenSelector.getComboBox().getSelectionModel().selectedItemProperty().subscribe((previous, current) -> {
			if (previous != current) screenModel.setStageScreen(current);
		});
	}

	private Node createScreenSelector() {
		final var toolBar = new ToolBar(screenSelector);
		toolBar.widthProperty()
				.subscribe(width -> screenSelector.fillWidth(width.doubleValue()
						- toolBar.getPadding().getLeft()
						- toolBar.getPadding().getRight()));
		return toolBar;
	}

}
