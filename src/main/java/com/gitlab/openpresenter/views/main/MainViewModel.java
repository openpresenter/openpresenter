/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.main;

import org.springframework.stereotype.Component;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.stage.Screen;
import lombok.NonNull;

@Component
public class MainViewModel {

	private final ObjectProperty<Screen> screen = new SimpleObjectProperty<>(Screen.getPrimary());

	private final BooleanProperty darkMode = new SimpleBooleanProperty();

	private final DoubleProperty projectDividerPos = new SimpleDoubleProperty();

	private final DoubleProperty previewDividerPos = new SimpleDoubleProperty();

	private final DoubleProperty libraryDividerPos = new SimpleDoubleProperty();

	public ObjectProperty<Screen> screenPropertiy() {
		return screen;
	}

	public Screen getScreen() {
		return screen.get();
	}

	public void setScreen(@NonNull final Screen screen) {
		this.screen.set(screen);
	}

	public BooleanProperty darkModeProperty() {
		return darkMode;
	}

	public boolean isDarkMode() {
		return darkMode.get();
	}

	public void setDarkMode(final boolean darkMode) {
		this.darkMode.set(darkMode);
	}

	public DoubleProperty projectDividerPosProperty() {
		return projectDividerPos;
	}

	public double getProjectDividerPos() {
		return projectDividerPos.get();
	}

	public void setProjectDividerPos(final double pos) {
		this.projectDividerPos.set(pos);
	}

	public DoubleProperty previewDividerPosProperty() {
		return previewDividerPos;
	}

	public double getPreviewDividerPos() {
		return previewDividerPos.get();
	}

	public void setPreviewDividerPos(final double pos) {
		this.previewDividerPos.set(pos);
	}

	public DoubleProperty libraryDividerPosProperty() {
		return libraryDividerPos;
	}

	public double getLibraryDividerPos() {
		return libraryDividerPos.get();
	}

	public void setlibraryDividerPos(final double pos) {
		this.libraryDividerPos.set(pos);
	}

}
