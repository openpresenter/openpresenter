/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.main;

import org.kordamp.ikonli.bootstrapicons.BootstrapIcons;
import org.kordamp.ikonli.javafx.FontIcon;
import org.springframework.stereotype.Component;

import atlantafx.base.controls.ToggleSwitch;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import lombok.NonNull;

@Component
public class MainViewToolBar extends BorderPane {

	private final ToggleSwitch darkModeSwitch;

	private final FontIcon darkModeIcon;

	public MainViewToolBar(@NonNull final MainViewModel model) {
		darkModeSwitch = new ToggleSwitch();
		darkModeSwitch.setTooltip(new Tooltip("Dark Mode"));
		Bindings.bindBidirectional(darkModeSwitch.selectedProperty(), model.darkModeProperty());

		darkModeIcon = new FontIcon(BootstrapIcons.MOON);

		final var vbox = new HBox(darkModeSwitch, darkModeIcon);
		vbox.setAlignment(Pos.CENTER_RIGHT);
		vbox.setPadding(new Insets(4));

		setCenter(wrapToolBar());
		setRight(wrapToolBar(vbox));

		darkModeSwitch.selectedProperty().subscribe(darkMode -> darkModeIcon.setIconCode(darkMode ? BootstrapIcons.MOON : BootstrapIcons.SUN));
	}

	private Node wrapToolBar(final Node... nodes) {
		final var toolBar = new ToolBar(nodes);
		toolBar.minHeightProperty().bind(heightProperty());
		return toolBar;
	}

}
