/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.main;

import org.springframework.stereotype.Component;

import atlantafx.base.theme.Styles;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import lombok.NonNull;

@Component
public class PreviewPane extends GridPane {

	public PreviewPane(
			@NonNull final AudiencePreviewPane audiencePreviewPane,
			@NonNull final StagePreviewPane stagePreviewPane) {

		getStyleClass().add(Styles.BG_INSET);

		final var audiencePreviewTab = new Tab("Audience", audiencePreviewPane);
		audiencePreviewTab.setClosable(false);

		final var audiencePreviewTabPane = new TabPane(audiencePreviewTab);
		audiencePreviewTabPane.getStyleClass().add(Styles.DENSE);
		add(audiencePreviewTabPane, 0, 0);

		final var stagePreviewTab = new Tab("Stage", stagePreviewPane);
		stagePreviewTab.setClosable(false);
		
		final var stagePreviewTabPane = new TabPane(stagePreviewTab);
		stagePreviewTabPane.getStyleClass().add(Styles.DENSE);
		add(stagePreviewTabPane, 0, 1);

		getChildren().forEach(node -> {
			setHgrow(node, Priority.ALWAYS);
			setVgrow(node, Priority.ALWAYS);
		});
	}

}
