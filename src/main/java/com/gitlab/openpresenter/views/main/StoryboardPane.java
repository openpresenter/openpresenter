/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.main;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.model.presentation.LyricsPresentationItem;
import com.gitlab.openpresenter.model.presentation.MediaPresentationItem;
import com.gitlab.openpresenter.model.presentation.SlidesPresentationItem;
import com.gitlab.openpresenter.views.presentation.PresentationViewModel;

import atlantafx.base.theme.Styles;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class StoryboardPane extends BorderPane {

	private final FlowPane contentPane;

	public StoryboardPane() {
		contentPane = new FlowPane();
		contentPane.setMinSize(200, 200);

		final var tab = new Tab("Storyboard", contentPane);
		tab.setClosable(false);

		final var tabPane = new TabPane(tab);
		tabPane.getStyleClass().add(Styles.DENSE);
		setCenter(tabPane);
	}

	@Autowired
	void setPresentationViewModel(@NonNull final PresentationViewModel presentationViewModel) {
		final var lyricsButton = new javafx.scene.control.Button("Lyrics");
		lyricsButton.setOnAction(event -> presentationViewModel.setCurrentItem(createLyricsSample()));

		final var slidesButton = new Button("Slides");
		slidesButton.setOnAction(event -> presentationViewModel.setCurrentItem(createSlidesSample()));

		final var videoButton = new Button("Video");
		videoButton.setOnAction(event -> presentationViewModel.setCurrentItem(createVideoSample()));

		final var clearButton = new Button("Clear");
		clearButton.setOnAction(event -> presentationViewModel.setCurrentItem(null));

		contentPane.getChildren().addAll(lyricsButton, slidesButton, videoButton, clearButton);
	}

	private LyricsPresentationItem createLyricsSample() {
		return LyricsPresentationItem.builder()
				.title("Go Tell It On The Mountain")
				.lines(new String[] {
						"Go tell it on the mountain",
						"Over the hills and ev'rywhere",
						"Go tell it on the mountain",
						"That Jesus Christ is born"
				})
				.build();
	}

	private SlidesPresentationItem createSlidesSample() {
		return SlidesPresentationItem.builder()
				.title("Psalm 23")
				.lines(new String[] {
						"The Lord is my shepherd; I shall not want.",
						"He makes me lie down in green pastures.",
						"He leads me beside still waters.",
						"He restores my soul.",
						"He leads me in paths of righteousness[2] for his name’s sake."
				})
				.build();
	}

	private MediaPresentationItem createVideoSample() {
		final var source = new File("/Volumes/Transfer/Jahreslosung2024/Alles in Liebe - Lied zur Jahreslosung 2024.mp4");

		final var media = new Media(source.toURI().toString());

		final var mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setAutoPlay(true);
		mediaPlayer.setOnError(() -> log.error("media playback failed", mediaPlayer.getError()));

		return MediaPresentationItem.builder()
				.mediaPlayer(mediaPlayer)
				.build();
	}

//	@Autowired
//	public void setMediaPlayerViewModel(@NonNull final MediaPlayerViewModel model, @NonNull final PresentationViewModel presentationViewModel) {
//		final var source = new File("/Volumes/Transfer/Jahreslosung2024/Alles in Liebe - Lied zur Jahreslosung 2024.mp4");
//
//		final var playBtn = new Button("Play");
//		playBtn.setOnAction(evt -> {
//			model.play(new Media(source.toURI().toString()));
//			presentationViewModel.setCalibrationVisible(false);
//		});
//
//		final var stopBtn = new Button("Stop");
//		stopBtn.setOnAction(evt -> model.play(null));
//
//		contentPane.getChildren().addAll(playBtn, stopBtn);
//	}

}
