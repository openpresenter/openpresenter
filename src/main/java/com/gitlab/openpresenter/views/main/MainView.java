/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter.views.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

@Component
public class MainView extends Stage {

	@Autowired
	private MainViewModel model;

	@Autowired
	private MainViewToolBar toolbar;

	@Autowired
	private SchedulePane schedulePane;

	@Autowired
	private StoryboardPane slidesPane;

	@Autowired
	private PreviewPane previewPane;

	@Autowired
	private LibraryPane libraryPane;

	private BorderPane contentPane;

	public MainView() {
		setOnCloseRequest(evt -> Platform.exit());
	}

	@PostConstruct
	void init() {
		model.screenPropertiy().subscribe(this::showOnScreen);

		final var hsplit = new SplitPane(schedulePane, slidesPane, previewPane);
		Bindings.bindBidirectional(hsplit.getDividers().get(0).positionProperty(), model.projectDividerPosProperty());
		Bindings.bindBidirectional(hsplit.getDividers().get(1).positionProperty(), model.previewDividerPosProperty());

		final var vsplit = new SplitPane(hsplit, libraryPane);
		vsplit.setOrientation(Orientation.VERTICAL);
		Bindings.bindBidirectional(vsplit.getDividers().get(0).positionProperty(), model.libraryDividerPosProperty());

		contentPane = new BorderPane();
		contentPane.setTop(toolbar);
		contentPane.setCenter(vsplit);

		setScene(new Scene(contentPane));
	}

	private void showOnScreen(final Screen screen) {
		setX(screen.getVisualBounds().getMinX());
		setY(screen.getVisualBounds().getMinY());
		setWidth(screen.getVisualBounds().getWidth());
		setHeight(screen.getVisualBounds().getHeight());
		show();
	}

}
