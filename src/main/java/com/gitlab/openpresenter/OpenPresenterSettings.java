/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.openpresenter;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.springframework.stereotype.Component;

import com.gitlab.openpresenter.model.screen.ScreenRatio;

import jakarta.annotation.PostConstruct;

@Component
public class OpenPresenterSettings {

	private static final String DARK_MODE = "DARK_MODE";

	private static final String SCREEN_RATIO = "SCREEN_RATIO";

	private static final String SCHEDULE_DIVIDER_POS = "SCHEDULE_DIVIDER_POS";

	private static final String PREVIEW_DIVIDER_POS = "PREVIEW_DIVIDER_POS";

	private static final String LIBRARY_DIVIDER_POS = "LIBRARY_DIVIDER_POS";

	private final Preferences preferences = Preferences.userNodeForPackage(getClass());

	@PostConstruct
	void init() throws BackingStoreException {
		// preferences.clear();
	}

	public boolean isDarkMode() {
		return preferences.getBoolean(DARK_MODE, true);
	}

	public void setDarkMode(final boolean darkMode) {
		preferences.putBoolean(DARK_MODE, darkMode);
	}

	public ScreenRatio getScreenRatio() {
		try {
			final var s = preferences.get(SCREEN_RATIO, null);
			return s != null ? ScreenRatio.valueOf(s) : ScreenRatio._16x9;
		} catch (final IllegalArgumentException e) {
			return ScreenRatio._16x9;
		}
	}

	public void setScreenRatio(final ScreenRatio screenRatio) {
		preferences.put(SCREEN_RATIO, screenRatio != null ? screenRatio.name() : null);
	}

	public double getScheduleDividerPos() {
		return preferences.getDouble(SCHEDULE_DIVIDER_POS, 0.2);
	}

	public void setScheduleDividerPos(final double pos) {
		preferences.putDouble(SCHEDULE_DIVIDER_POS, pos);
	}

	public double getPreviewDividerPos() {
		return preferences.getDouble(PREVIEW_DIVIDER_POS, 0.8);
	}

	public void setPreviewDividerPos(final double pos) {
		preferences.putDouble(PREVIEW_DIVIDER_POS, pos);
	}

	public double getLibraryDividerPos() {
		return preferences.getDouble(LIBRARY_DIVIDER_POS, 0.75);
	}

	public void setLibraryDividerPos(final double pos) {
		preferences.putDouble(LIBRARY_DIVIDER_POS, pos);
	}

}
